# k-Nearest Neighbors regression case study

A tour through how to use [Cottonwood's](https://e2eml.school/cottonwood)
[k-nearest neighbors](https://youtu.be/KluQCQtHTqk) (kNN) algorithm to
predict diamond prices based on a diamond's characteristics using this
[diamond prices data set](https://gitlab.com/brohrer/cottonwood-data-diamonds).

## `a_predict_prices.py`

This script demonstrates Cottonwood's online, incremental implementation of
k-nearest neighbors. It iterates through the entire diamonds data set,
at each step using all the previously observed diamonds to predict
the price of the next.

Because the prices vary over orders of magnitude, error is reported
in percent, rather than absolute dollars.

Here is a typical learning curve, showing how error decreases with more data.
Using a k of 7, the error tends to settle in at about 9%.
If you want to play around with this, you can adjust k and see how that
affects the result.

![](learning_curve_a.png)


For an in-depth walk through of k nearest neighbors theory and implementation,
check out [End to End Machine Learning Course 221](https"//e2eml.school/221).

## `b_learn_weights.py` 

This script enables the learning of feature weights. By default,
features are automatically scaled to have a standard deviation of one.
This makes the assumption that all features are independent and
of equal importance. In practice, but this is rarely the case.
The ability to learn feature weights lets the model adapt to cases
where some features may be redundant or provide a little useful information.

The process for learning feature weights is described
[here](https://e2eml.school/knn_adaptive_feature_weighting.html),
and developed in detail in [e2eML Course 221](https:/e2eml.school/221).
It is computationally intensive since it relies on
leave-one-out cross validation to evaluate each change in weights.
Once you start this running, it can take days to work all the way through
the data set. However, it starts making improvements to the weights
immediately, and it saves out the best weights it has found so far as it goes.
The best-so-far weights are updated into the file `feature_weights.csv`.
Most of the improvement comes early on, so don’t be put off by
the long run time for this.

If you want to skip straight to the end, a set of learned feature weights
on the whole data set is stored in `feature_weights.csv.learned`.
You can use it by copying it,

```bash
$ cp feature_weights.csv.learned feature_weights.csv
```

Taking a look at the learned feature weights, we can see that they deform
the feature space dramatically.

- 5.2
- 0.15
- 1.1
- 5.0
- 0.061
- 0.043
- 9.3
- 10.6
- 0.00000000001


Some are greater than 10, and some are so small has to be effectively zero.
This is a strong signal about which features have the most influence
on the prediction results.

## `c_use_trained_weights.py`

This script shows how to kick off a run through the data that makes use
of previously learned feature weights. Running it, we can see that
getting the right feature weights brings our error down to about 7%,
a noteworthy improvement.

![](learning_curve_c.png)

## `d_early_stopping.py`

The computational bottleneck of k-nearest neighbors is calculating
the distance between the point being evaluated and all of the previously
observed points. If you want to speed up inference, limiting the number of
points you keep is the most straightforward way. This is analogous to early
stopping in machine learning where you choose to stop updating your model,
even though you still have training data available.

In this implementation, the total number of data points is limited,
but each new data point does have a chance of being swapped in to the set of
keepers. This accounts for the case where the input data is not well
randomized and prevents the model from being biased toward one small part
of the feature space.

Varying the number of data points retained shows just how much mileage
k-nearest neighbors can squeeze out of its first few observations. 

|Stopping at iteration | Final error (percent) | Time to run (s)|
--- | --- | ---
|53,000 (full)|7|120|
|20,000|8|70|
|10,000|9|38|
|4,000|10|18|
|2,000|11|11|
|1,000|12|8|
|400|13.5|5.5|
|200|15|4.5|
|100|18|4.1|
|50|21.5|3.7|


## `e_data_reduction.py`

Randomly selecting a subset of points to keep is an effective method,
but it’s tempting to try to improve on it. Finding an general purpose way
to do this is an open research question, but Cottonwood's k-nearest neighbor
block uses [this method](https://e2eml.school/knn_data_reduction.html)
that I stumbled onto after some experimentation.

It turns out that k-nearest neighbors is so naturally robust that
it’s hard to improve upon randomly choosing a subset of your points
for data reduction. For this particular data set, we do see an improvement
over random selection of about half a percent across a wide range
of model sizes. As the model gets smaller, the advantage appears to grow.
I expect that the results will vary considerably with the nature of each
problem and data set. The training time for this active data reduction
method is a little more than twice that of the random subsetting.

|Stopping at iteration | Final error (percent) | Improvement (percent) |
--- | --- | ---
| 10,000 |  8.5 | .5 |
|  4,000 |  9.5 | .5 |
|  2,000 | 10.5 | .5 |
|  1,000 | 11.5 | .5 |
|    400 | 13   | .5 |
|    200 | 14.5 | .5 |
|    100 | 17   | 1  |
|    50  | 20   | 1.5|

